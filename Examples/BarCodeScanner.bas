REM Start of BASIC! Include Program BarCodeScanner.bas

FN.DEF GetBarCode(SCAN_MODE$,globals)
 BUNDLE.CONTAIN globals,"barcodeFirstRun",barcodeFirstRun
 IF barcodeFirstRun = 0
  BUNDLE.PUT globals,"barcodeFirstRun",1
  APP.INSTALLED check, "com.google.zxing.client.android"
  IF check = 0
   APP.LOAD "market://details?id="+ "com.google.zxing.client.android"
   DIALOG.MESSAGE "Installing succeeded?", "Please confirm", buttonResult, "Yes", "No"
   IF buttonResult = 2
    BUNDLE.PUT globals,"barcodeFirstRun",0
   ENDIF
  ENDIF

  !SCAN_MODEs$ = "QR_CODE_MODE,CODE_39,CODE_128,EAN_8,EAN_13,ITF,UPC_A,UPC_E,DATAMATRIX,PDF417,QR_CODE"
  !"UPC_A", "UPC_E", "EAN_8", "EAN_13","RSS_14","UPC_A","UPC_E","EAN_8","EAN_13","CODE_39","CODE_93","CODE_128","ITF","RSS_14","RSS_EXPANDED","QR_CODE",
  LIST.CREATE S, commandListPointer
  LIST.ADD commandListPointer~
  "new Intent("+CHR$(34)+"com.google.zxing.client.android.SCAN"+CHR$(34)+");" ~

  ! "putExtra ("+CHR$(34)+"SCAN_MODE"+CHR$(34)+","+CHR$(34)+ "QR_CODE_MODE"+CHR$(34)+");" ~
  "putExtra("+CHR$(34)+"PROMPT_MESSAGE"+CHR$(34)+","+CHR$(34)+ "Scan the Item!"+CHR$(34)+");" ~
  "addCategory(Intent.CATEGORY_DEFAULT);" ~
  "addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);" ~
  "addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);" ~
  "EOCL"


  LIST.CREATE S, resultListPointer
  LIST.ADD resultListPointer~
  "result$=getStringExtra("+CHR$(34)+"SCAN_RESULT"+CHR$(34)+");" ~
  "format$=getStringExtra("+CHR$(34)+"SCAN_RESULT_FORMAT"+CHR$(34)+");" ~
  "errCorrection$=getStringExtra("+CHR$(34)+"SCAN_RESULT_ERROR_CORRECTION_LEVEL"+CHR$(34)+");" ~
  "i#intentOrientation=getIntExtra("+CHR$(34)+"SCAN_RESULT_ORIENTATION"+CHR$(34)+",-5);" ~ %You get 180 for bar codes reading from right to left.
  "EORL"

  !   byte[] rawBytes = intent.getByteArrayExtra("SCAN_RESULT_BYTES");

  !  BUNDLE.CREATE appVarPointer
  ! May be later
  ! BUNDLE.PL or BUNDLE.PUT.LIST appVarPointer,"***CommandList***",commandListPointer
  BUNDLE.PL appVarPointer,"***CommandList***",commandListPointer
  BUNDLE.PL appVarPointer,"***ResultList***",resultListPointer
  BUNDLE.PUT appVarPointer,"result$","nothing"
  BUNDLE.PUT appVarPointer,"format$",""
  BUNDLE.PUT appVarPointer,"errCorrection$",""
  BUNDLE.PUT appVarPointer,"i#intentOrientation",-1
  ! May be later
  ! BUNDLE.PB or BUNDLE.PUT.BUNDLE appVarPointer,"myBundle",myBundlePointer

  BUNDLE.PUT globals,"barcodeAppVarPointer",appVarPointer
 ELSE
  BUNDLE.GET globals,"barcodeAppVarPointer",appVarPointer
  BUNDLE.PUT appVarPointer,"result$","nothing"
 ENDIF

 APP.SAR appVarPointer
 ! May be later
 ! APP.SAR appVarPointer, commandListPointer, resultListPointer
 FN.RTN appVarPointer

FN.END

FN.DEF GetBarCodeSimple$(globals)
 ResultPointer = GetBarCode("",globals)
 BUNDLE.GET ResultPointer, "result$", result$
 BUNDLE.GET ResultPointer, "i#intentOrientation", intentOrientation
 PRINT "i#intentOrientation", intentOrientation
 FN.RTN result$
FN.END

DEBUG.OFF

BUNDLE.CREATE globals

	!!
	PRINT "Scan Result: " ; GetBarCodeSimple$(globals) %Works twice! Issue #79
	!!

myBarcode$ = GetBarCodeSimple$(globals)
PRINT "Scan Result: " ; myBarcode$

DIALOG.MESSAGE "", "Go on", buttonResult, "Go"

myBarcode$ = GetBarCodeSimple$(globals)
PRINT "Scan Result: " ; myBarcode$

END
