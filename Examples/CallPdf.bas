REM Start of BASIC! Include Program CallAnActivity.bas

FN.DEF ViewPdf(fileName$)

 LIST.CREATE S, commandListPointer
 LIST.ADD commandListPointer~
 "new Intent("+CHR$(34)+"Intent.ACTION_VIEW"+CHR$(34)+");" ~
 "setPackage("+CHR$(34)+"com.adobe.reader"+CHR$(34)+");" ~
 "setDataAndType("+CHR$(34)+fileName$+CHR$(34)+","+CHR$(34)+ "application/pdf"+CHR$(34)+");" ~
 "addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);" ~
 "addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);" ~
 "EOCL"

 !BUNDLE.CREATE appVarPointer % Obsolet because BUNDLE Auto-Create
 BUNDLE.PL appVarPointer,"***CommandList***",commandListPointer

 APP.SAR appVarPointer

 FN.RTN appVarPointer

FN.END


  file.root dataPath$
  fileName$ = dataPath$ + "/" + "App.SAR FlowChart 2016-05-30.pdf"
  PRINT "pdfFileName: " , fileName$
  myVarPointer = ViewPdf (fileName$)

END
